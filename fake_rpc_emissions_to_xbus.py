from zmq_rpc.client import ZmqRpcClient


# Connection to xbus
conn = ZmqRpcClient('tcp://127.0.0.1:1984', timeout=1000)

token = conn.login('etudes_django_emitter', 'Ecei5vor')
envelope_id = conn.start_envelope(token)
event_id = conn.start_event(token, envelope_id,
                            'etudes_django_frontend_profile_updated', 0)
item = conn.packer.pack({})
conn.send_item(token, envelope_id, event_id, item)
conn.end_event(token, envelope_id, event_id)
conn.end_envelope(token, envelope_id)
