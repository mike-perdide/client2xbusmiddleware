# * Authors:
#       * Julien Miotte <j.m@majerti.fr>;

# Import from libs
import pyramid
from zmq_rpc.client import ZmqRpcClient

connection_list = {}


def get_or_create_connection(user):
    settings = pyramid.threadlocal.get_current_registry().settings
    front_url = settings['xbus.front_url']
    consumer_url = settings['xbus.consumer_url']
    local_listen_url = settings['listener_url']

    (conn, token, consumer_token) = connection_list.get(user.htoken,
                                                        (None, '', ''))
    if conn is None:
        print(
            "Creating connection and registering consumer for user.",
        )
        # Create connection to Xbus to forward events
        conn = ZmqRpcClient(front_url, timeout=1000)
        token = conn.login(user.emitter_login, user.emitter_password)

        connection_list[user.htoken] = (conn, token, consumer_token)

        # Register consumer to Xbus
        consumer_client = ZmqRpcClient(consumer_url, timeout=1000)
        consumer_token = consumer_client.login(user.consumer_login,
                                               user.consumer_password)
        consumer_client.register_node(consumer_token, local_listen_url)
    else:
        # is conn valid ?
        # if not close, reopen
        pass
    return conn, token, consumer_token
