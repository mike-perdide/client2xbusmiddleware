# * Authors:
#       * Julien Miotte <j.m@majerti.fr>;

# Import from stdlib
import asyncio
from datetime import datetime

# Import from libs
import msgpack
from aiozmq import rpc

# Import from SQLAlchemy
from sqlalchemy.orm import scoped_session, sessionmaker

# Import from this project
from middleware.models import User
from middleware.client_consumers import RESTConsumer


DBSession = scoped_session(sessionmaker())


consumer_client_dict = {}


def get_client_from_token(token):
    user = DBSession.query(User).filter_by(consumer_token=token).one()

    if user.htoken in consumer_client_dict:
        client = consumer_client_dict[user.htoken]
    else:
        # New client connection depending on client consumer type
        # FIXME Use client_consumers.CONSUMER_TYPE_DICT to create proper
        # consumer class instance
        client = RESTConsumer(user)
        consumer_client_dict[user.htoken] = client

    return client


class ConsumerServer(rpc.AttrHandler):
    """aiozmq server that implements the Xbus emitter API; the data is sent as
    is to the Xbus broker front-end.
    """

    def __init__(self, url: str, zmq_loop, login, password):
        """Initialize the server.
        """

        self.url = url
        self.zmq_loop = zmq_loop

        super(ConsumerServer, self).__init__()

    @asyncio.coroutine
    def start(self):
        server = yield from rpc.serve_rpc(
            self, bind=self.url, loop=self.zmq_loop
        )
        print("Consumer server started on:", self.url)
        return server

    # RPC methods below are part of the Xbus emitter API.
    @rpc.method
    @asyncio.coroutine
    def get_metadata(self):
        print("Consumer: get_metadata")
        now = datetime.now().isoformat()
        return {
            u'name': u'django-events-consumer',
            u'version': 0.1,
            u'api_version': 0.1,
            u'host': u'localhost',
            u'start_date': now,
            u'local_time': now,
        }

    @rpc.method
    @asyncio.coroutine
    def has_clearing(self):
        print("Consumer: has_clearing")
        return False, None

    @rpc.method
    @asyncio.coroutine
    def has_immediate_reply(self):
        print("Consumer: has_immediate_reply")
        return False, None

    @rpc.method
    @asyncio.coroutine
    def event_started(self, token, envelope_id, event_id, type_name):
        print("Consumer: event started", type_name)
        # Find client from envelope_id?
        client = get_client_from_token(token)
        return True, client.event_started(envelope_id, event_id, type_name)

    @rpc.method
    @asyncio.coroutine
    def item_received(self, token, envelope_id, event_id, indices, data):
        print("Consumer: item received")
        client = get_client_from_token(token)
        # unpack data
        item = msgpack.unpackb(data, encoding='utf-8')
        return True, client.item_received(envelope_id, event_id, indices, item)

    @rpc.method
    @asyncio.coroutine
    def event_ended(self, token, envelope_id, event_id):
        print("Consumer: event ended")
        client = get_client_from_token(token)
        return True, client.event_ended(envelope_id, event_id)

    @rpc.method
    @asyncio.coroutine
    def envelope_ended(self, token, envelope_id):
        print("Consumer: envelope ended")
        client = get_client_from_token(token)
        return True, client.envelope_ended(envelope_id)
