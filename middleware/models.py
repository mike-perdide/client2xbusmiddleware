# * Authors:
#       * Julien Miotte <j.m@majerti.fr>;

# Import from SQLAlchemy
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, Integer, String, ForeignKey, Enum
from sqlalchemy.orm import (
    scoped_session, sessionmaker, relationship,
)

# Import from this project
from middleware.client_consumers import CONSUMER_TYPE_DICT

DBSession = scoped_session(sessionmaker())
Base = declarative_base()


def create(model, **kwargs):
    try:
        instance = model(**kwargs)
        DBSession.add(instance)
        DBSession.commit()
    except:
        DBSession.rollback()
        raise
    return instance


def delete(instance):
    DBSession.delete(instance)
    DBSession.commit()


class User(Base):
    __tablename__ = 'users'
    htoken = Column(String, primary_key=True)
    name = Column(String)
    emitter_login = Column(String)
    emitter_password = Column(String)
    emitter_token = Column(String)

    consumer_login = Column(String)
    consumer_password = Column(String)
    consumer_token = Column(String)
    listener_address = Column(String)
    listener_port = Column(Integer)
    listener_prefix = Column(String, default='/')

    envelopes = relationship("Envelope",
                             foreign_keys="Envelope.user_id",
                             backref="user")
    consumer_type = Column(Enum(*CONSUMER_TYPE_DICT.keys()))


class QueueItem(Base):
    __tablename__ = 'queue_items'
    id = Column(String, primary_key=True)
    # xbus-bound, client-bound
    direction = Column(String)
    fail_count = Column(Integer)
    # pending, done, error
    state = Column(String)


class Envelope(QueueItem):
    __tablename__ = 'envelopes'
    id = Column(String, ForeignKey(QueueItem.id), primary_key=True)
    user_id = Column(String, ForeignKey(User.htoken))
    events = relationship("Event",
                          foreign_keys="Event.envelope_id",
                          backref="envelope")


class Event(QueueItem):
    __tablename__ = 'events'
    id = Column(String, ForeignKey(QueueItem.id), primary_key=True)
    envelope_id = Column(String, ForeignKey(Envelope.id))
