# * Authors:
#       * Julien Miotte <j.m@majerti.fr>;

# Import from stdlib
import os
import json
import binascii

# Import from libs
from webob import Response, exc

# Import from this project
from middleware.models import (
    DBSession, User, Envelope, Event,
)
from middleware.connection import get_or_create_connection


def _create_token():
    return binascii.b2a_hex(os.urandom(20))


class _401(exc.HTTPError):
    def __init__(self, msg='Unauthorized'):
        body = {'status': 401, 'message': msg}
        Response.__init__(self, json.dumps(body))
        self.status = 401
        self.content_type = 'application/json'


def get_message_from_request(request):
    try:
        body = request.body.decode('utf-8')
        message = json.loads(body)
    except ValueError:
        request.errors.add('body', 'message', 'Not valid JSON')
        return

    return message


def valid_user(request):
    header = 'X-Messaging-Token'
    htoken = request.headers.get(header)
    if htoken is None:
        raise _401(msg="Please provide an authentication token")

    user = DBSession.query(User).get(htoken)
    if user is None:
        raise _401("Incorrect authentication token")

    request.validated['user'] = user
    conn, emitter_token, consumer_token = get_or_create_connection(user)
    emitter_token = '12'
    if emitter_token == '':
        raise _401("Incorrect emitter credential config on the middleware")
    consumer_token = '12'
    if consumer_token == '':
        raise _401("Incorrect consumer credential config on the middleware")

    user.emitter_token = emitter_token
    user.consumer_token = consumer_token
    DBSession.add(user)
    DBSession.commit()

    request.validated['conn_token'] = conn, emitter_token


def valid_envelope_id(request):
    """
        Envelope ID validator
    """
    message = get_message_from_request(request)

    if 'envelope_id' not in message:
        request.errors.add('body', 'envelope_id', 'Missing envelope id')
        return

    envelope_id = message['envelope_id']
    envelope = DBSession.query(Envelope).get(envelope_id)
    if envelope is None:
        request.errors.add('body', 'envelope_id', 'Unknown envelope id')
        return

    user = request.validated['user']
    if envelope.user != user:
        request.errors.add(
            'body', 'envelope_id',
            'You are not authorized to close this envelope.')
        return

    request.validated['envelope'] = envelope


def valid_event_type(request):
    message = get_message_from_request(request)

    if 'event_type' not in message:
        request.errors.add('body', 'event_type', 'Missing event type')
        return

    request.validated['event_type'] = message['event_type']


def valid_event_id(request):
    """
        Event ID validator
    """
    message = get_message_from_request(request)

    if 'event_id' not in message:
        request.errors.add('body', 'event_id', 'Missing event id')
        return

    event_id = message['event_id']
    event = DBSession.query(Event).get(event_id)
    if event is None:
        request.errors.add('body', 'event_id', 'Unknown event id')
        return

    envelope = request.validated['envelope']
    if event.envelope != envelope:
        request.errors.add(
            'body', 'event_id',
            'This event_id is not associated with this envelope_id')
        return

    request.validated['event'] = event


def valid_item(request):
    """
        Item validator
    """
    message = get_message_from_request(request)

    if 'item' not in message:
        request.errors.add('body', 'item', 'Missing item')
        return

    request.validated['item'] = message['item']


def valid_full_envelope(request):
    message = get_message_from_request(request)

    if 'multiple_event_dict' not in message:
        request.errors.add('body', 'multiple_event_dict', 'Missing event dict')
        return

    request.validated['multiple_event_dict'] = message['multiple_event_dict']


def valid_full_event(request):
    message = get_message_from_request(request)

    if 'multiple_item_list' not in message:
        request.errors.add('body', 'multiple_item_list', 'Missing item list')
        return

    request.validated['multiple_item_list'] = message['multiple_item_list']
