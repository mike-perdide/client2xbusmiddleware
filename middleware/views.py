# * Authors:
#       * Julien Miotte <j.m@majerti.fr>;

""" Cornice services.
"""
# Import from libs
import msgpack

# Import from cornice
from cornice import Service
from cornice.resource import view, resource

# Import from this project
from middleware.validators import (
    valid_user, valid_envelope_id, valid_event_type, valid_event_id,
    valid_item, valid_full_envelope, valid_full_event,
)
from middleware.models import (
    Envelope, Event, create, delete,
)


#@resource(collection_path='/user', path='/user/{id}')
#class User(object):
#
#    def __init__(self, request):
#        self.request = request
#
#    @view(validators=valid_token)
#    def collection_get(self):
#        """Returns a list of all user"""
#        return {'users': _USERS.keys()}
#
#    @view(validators=unique)
#    def collection_post(self):
#        """Adds a new user"""
#        user = self.request.validated['user']
#        _USERS[user['name']] = user['token']
#        return {'token': '%s-%s' % (user['name'], user['token'])}
#
#    @view(validators=valid_token)
#    def collection_delete(self):
#        """Removes the user."""
#        name = self.request.validated['user']
#        del _USERS[name]
#        return {'Goodbye': name}


xbus_full_envelope = Service(name='xbus_full_envelope',
                             path='/full_envelope',
                             description="send multiple events at once")


@xbus_full_envelope.post(validators=(valid_user, valid_full_envelope))
def full_envelope(request):
    """
        Send multiple events at once
    """
    print("Mdlwre: multi events")
    conn, token = request.validated["conn_token"]
    event_item_dict = request.validated["multiple_event_dict"]
    # start_envelope
    envelope_id = conn.start_envelope(token)
    for event_type, xbus_item_dict_list in event_item_dict.items():
        # start_event
        event_id = conn.start_event(token, envelope_id, event_type, 0)
        for xbus_item_dict in xbus_item_dict_list:
            # send_item
            item = conn.packer.pack(xbus_item_dict)
            conn.send_item(token, envelope_id, event_id, item)

        # end_event
        conn.end_event(token, envelope_id, event_id)

    # end_envelope
    conn.end_envelope(token, envelope_id)


xbus_full_event = Service(name='xbus_full_event',
                          path='/full_event/{event_type}',
                          description='')


@xbus_full_event.post(validators=(valid_user, valid_full_event))
def full_event(request):
    """
        Send multiple items at once
    """
    conn, token = request.validated["conn_token"]
    event_type = request.matchdict['event_type']
    event_item_list = request.validated["multiple_item_list"]
    # start_envelope
    envelope_id = conn.start_envelope(token)
    # start_event
    event_id = conn.start_event(token, envelope_id, event_type, 0)
    for xbus_item_dict in event_item_list:
        # send_item
        item = conn.packer.pack(xbus_item_dict)
        conn.send_item(token, envelope_id, event_id, item)

    # end_event
    conn.end_event(token, envelope_id, event_id)

    # end_envelope
    conn.end_envelope(token, envelope_id)


@resource(collection_path='/envelope', path='/envelope/{id}')
class EnvelopeResource(object):
    def __init__(self, request):
        self.request = request

    @view(renderer='json', accept='text/json',
          validators=valid_user)
    def collection_post(self):
        # Create envelope
        print("Mdlwre: starting envelope")
        request = self.request
        user = request.validated['user']
        conn, token = request.validated['conn_token']

        envelope_id = conn.start_envelope(token)
        if not envelope_id:
            request.errors.add("body", "message", "Couldn't add envelope")
            return
        envelope = create(Envelope, id=envelope_id, user=user)

        return {"envelope_id": envelope.id}

    @view(renderer='json', accept='text/json',
          validators=(valid_user, valid_envelope_id,))
    def collection_put(self):
        # Close envelope
        print("Mdlwre: starting envelope")
        request = self.request
        conn, token = request.validated['conn_token']
        envelope = request.validated['envelope']

        conn.end_envelope(token, envelope.id)
        delete(envelope)
        return {}


@resource(collection_path='/event', path='/event/{id}')
class EventResource(object):
    def __init__(self, request):
        self.request = request

    @view(renderer='json', accept='text/json',
          validators=(valid_user, valid_envelope_id, valid_event_type,))
    def collection_post(self):
        # Create event
        print("Mdlwre: creating event")
        request = self.request
        conn, token = request.validated['conn_token']
        envelope = request.validated['envelope']
        event_type = request.validated['event_type']

        event_id = conn.start_event(token, envelope.id, event_type, 0)
        if not event_id:
            request.errors.add("body", "message", "Couldn't add event")
            return
        event = create(Event, id=event_id, envelope=envelope)

        return {
            "envelope_id": envelope.id,
            "event_id": event.id,
        }

    @view(renderer='json', accept='text/json',
          validators=(valid_user, valid_envelope_id, valid_event_id,))
    def collection_put(self):
        # Close event
        print("Mdlwre: ending event")
        request = self.request
        conn, token = request.validated['conn_token']
        envelope = request.validated['envelope']
        event = request.validated['event']

        conn.end_event(token, envelope.id, event.id)
        delete(event)
        return {}


@resource(collection_path='/item', path='/item/{id}')
class ItemResource(object):
    def __init__(self, request):
        self.request = request

    @view(renderer='json', accept='text/json',
          validators=(valid_user, valid_envelope_id,
                      valid_event_id, valid_item,))
    def collection_post(self):
        # Create event
        print("Mdlwre: sending item")
        request = self.request
        conn, token = request.validated['conn_token']
        envelope = request.validated['envelope']
        event = request.validated['event']
        item = request.validated['item']

        item_id = conn.send_item(token, envelope.id, event.id, item)
        return {'item_id': item_id}


@resource(collection_path='/consumer', path='')
def ConsumerResource(object):
    def __init__(self, request):
        self.request = request
