# * Authors:
#       * Julien Miotte <j.m@majerti.fr>;

# Import from stdlib
import json
import http.client
from urllib.parse import urljoin

# Two client consumer types
# - the ones providing a REST server to receive events
# - the ones polling at intervals


class ClientConsumer(object):

    def event_started(self):
        pass

    def event_ended(self):
        pass

    def item_received(self):
        pass

    def envelope_ended(self):
        pass

    def full_envelope(self, event_dict):
        # {'event1': [{'xref': ..., ...}, {'xref': ..., ...}, ...],
        #  'event2': ...}
        pass

    def full_event(self, event_type, item_list):
        # [{'xref': ..., ...}, {'xref': ..., ...}]
        pass


class PollingConsumer(ClientConsumer):
    _consumer_type = "polling"

    # Provide full envelopes
    # Cannot achieve client//xbus sync unless we create step objects like
    # StartEnvelope, StartEvent, Send
    # Alternative: only sync to client on end event

    def event_started():
        # Store data
        # Release the caller
        pass

    def item_received():
        # Store data
        # Release the caller
        pass

    def event_ended():
        # Store data
        # Release the caller
        pass

    def envelope_ended():
        # Set envelope object as available for user
        # Hold the caller until we have confirmation that the event has been
        # consumed
        pass


class RESTConsumer(ClientConsumer):
    _consumer_type = "rest"

    # Directly contact the client consumer
    def __init__(self, user):
        self.address = user.listener_address
        self.port = user.listener_port
        self.prefix = user.listener_prefix
        self.headers = {"Content-type": "text/json"}

    def send_rest_request(self, param_dict, method, path):
        # FIXME, we should keep the connection and check if it's still alive
        params = json.dumps(param_dict)
        conn = http.client.HTTPConnection(self.address, port=self.port)
        conn.request(method, urljoin(self.prefix, path), params, self.headers)

        response = conn.getresponse()
        conn.close()
        data = response.read().decode('utf-8')
        data = json.loads(data)

        return data

    def event_started(self, envelope_id, event_id, event_type):
        param_dict = {
            'envelope_id': envelope_id,
            'event_id': event_id,
            'event_type': event_type,
        }
        try:
            self.send_rest_request(param_dict, 'POST', 'event')
        except Exception as err:
            print("RESTConsumer.event_started error:", err)

    def event_ended(self, envelope_id, event_id):
        param_dict = {
            'envelope_id': envelope_id,
            'event_id': event_id,
        }
        try:
            self.send_rest_request(param_dict, 'PUT', 'event')
        except Exception as err:
            print("RESTConsumer.event_ended error:", err)

    def item_received(self, envelope_id, event_id, indices, item):
        param_dict = {
            'envelope_id': envelope_id,
            'event_id': event_id,
            'indices': indices,
            'item': item,
        }
        try:
            self.send_rest_request(param_dict, 'POST', 'item')
        except Exception as err:
            print("RESTConsumer.item_received error:", err)

    def envelope_ended(self, envelope_id):
        param_dict = {
            'envelope_id': envelope_id,
        }
        try:
            self.send_rest_request(param_dict, 'POST', 'envelope')
        except Exception as err:
            print("RESTConsumer.item_received error:", err)



CONSUMER_CLASSES = [PollingConsumer, RESTConsumer]
CONSUMER_TYPE_DICT = dict([(cls._consumer_type, cls)
                           for cls in CONSUMER_CLASSES])
