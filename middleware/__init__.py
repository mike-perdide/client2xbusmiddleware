# * Authors:
#       * Julien Miotte <j.m@majerti.fr>;

"""Main entry point
"""

# Import from stdlib
import asyncio
import multiprocessing

# Import from libs
import aiozmq

# Import from Pyramid/SqlAlchemy
from pyramid.config import Configurator
from sqlalchemy import engine_from_config
from sqlalchemy.orm import scoped_session, sessionmaker

# Import from this project
from middleware.models import Base, User, create
from middleware.consumer_server import ConsumerServer


def dev_accounts():
    create(
        User,
        htoken='bcd1ed6f62ce3871ae5ccfa0a3d6944c735745c6',
        emitter_login='etudes_django_emitter',
        emitter_password='Ecei5vor',
        #consumer_login='test_consumer_role',
        #consumer_password='Ecei5vor',
        consumer_login='etudes_django_role',
        consumer_password='Ecei5vor',
        listener_address='127.0.0.1',
        listener_port='6555',
    )


def main(global_config, **settings):
    DBSession = scoped_session(sessionmaker())
    engine = engine_from_config(settings, 'sqlalchemy.')
    DBSession.configure(bind=engine)
    Base.metadata.bind = engine
    # Fixme, find something more elegant
    try:
        DBSession.query(User).all()
    except Exception:
        print("Creating database")
        Base.metadata.create_all(engine)
        # Create admin account
        # Create dev accounts
        dev_accounts()

    config = Configurator(settings=settings)
    config.include("cornice")
    config.scan("middleware.views")

    # Start consumer server
    zmq_loop = aiozmq.ZmqEventLoopPolicy().new_event_loop()
    consumer_server = ConsumerServer(
        settings['listener_url'],
        zmq_loop,
        login="",
        password="",
    )
    asyncio.async(consumer_server.start(), loop=zmq_loop)
    consumer_server_proc = multiprocessing.Process(target=zmq_loop.run_forever)
    consumer_server_proc.start()

    return config.make_wsgi_app()
