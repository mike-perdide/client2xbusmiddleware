Middleware is an XBus proxy aiming at offering a REST/Json API for several clients, and at relaying XBus messages to client consumers.

Requirements::

- cornice (1.0.0)
- msgpack-python (0.4.6)
- zmq-rpc (0.1)
- waitress (0.8.9)
- Sphinx (for documentation purposes, 1.3.0)

Launch with::

$ pserve middleware.ini

Test with::

$ python fake_rest_emissions.py  # to simulate REST/Json calls from a client
$ python fake_rpc_emissions_to_xbus.py  # to simulate RPC emissions to xbus that would be propagated to xbus registered consumers (this software)
$ pyton fake_xbus_rpc_calls.py  # to simulate direct RPC calls from xbus

There is also a fake cornice server to simulate a client consumer API, in client_test_server/. Launch with::

$ pserve client_test_server/client_test_server.ini


Components
==========

The program is initialized in *__init__.py*:main(), and creates all tables of the database if not found, then launches the REST server in the main thread and the RPC server in a separate process.

The **REST API** lives in *views.py*, as Cornice services or resources. Data validation (valid user, valid message, etc.) is done on most incoming messages and is described in validators.py.

The **RPC API** lives in *consumer_server.py*, as ZmqRpc methods.

The **User/Client configuration** is stored in a database, in a way described in *models.py*, and is meant to represent client configurations (token, preferred consumer method, credentials, ...). Users or clients authentify themselves by providing a token in the request headers, and this token will be matched against the User model to retrieve emitter and consumer credentials needed to register to Xbus. This is partly done is connection.py.

The different types of **client consumer handlers** are described in *client_consumers.py*. For now, we have two::

- REST client consumers, which expose a REST API for this program to transmit xbus messages to,
- and Polling client consumers, that periodically polls one of the REST server views (could be '/envelope_queue') to check if it has envelopes to consume.

Overall description
===================

Emission direction
------------------

Clients contact the middleware via REST/Json requests, on the three main views::

- /envelope (POST for starting an envelope, PUT to end/close it)
- /event (POST for starting an envelope, PUT to end/close it)
- /item (POST for sending an item)

With each request, the client provides an authentication token. On the first request, the program will use this token to retrieve emitter and consumer credentials from the User configuration model, and use it to open a connection to Xbus, log in, and register itself as a consumer for the client's role (providing its own consumer address on which a RPC Server is listening).

After data validation, the message is processed and transmitted directly via RPC call to Xbus, through the user's connection that was created on the first request and that is kept for all requests.

Two more views are available for user convenience::

- /full_envelope, to post a full envelope in one request (that should receive a dict with event_types as keys and item lists as values : {'event_type1': [item1, item2, ...], 'event_type_2': [item3, item4, ...]})
- /full_event, to post a full event in one request, leaving the creation of the envelope to the middleware (that should receive a list of items and an event_type)

Consumption direction
---------------------

The program offers the following RPC methods to XBus::

- event_started(token, envelope_id, event_id, event_type)
- item_received(token, envelope_id, event_id, indices, item)
- event_ended(token, envelope_id, event_id)
- envelope_ended(token, envelope_id)

XBus calls these RPC methods and provides the same token that was returned upon consumer login. Using this token we can retrieve the client consumer type (polling or rest for instance), and more importantly the client's consumer address (the token is acting as a mean of determining the routing of the data), and call the related handler to forward the data to the client. 


Missing
=======
::

- A /login view for client to inform the middleware that they are ready (without it we have to wait for the first REST/Json request
- Data validation on incoming RPC calls (from xbus)
- Proper return values and blocking behaviors to ensure envelope deliveries
- Proper behaviors when connections are failing or when erros in general are encountered
