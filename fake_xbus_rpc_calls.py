from zmq_rpc.client import ZmqRpcClient


print("Connect to the middleware directly and emulate xbus behavior")
# Connection to middleware
conn = ZmqRpcClient('tcp://127.0.0.2:4892', timeout=1000)

token = '12'
envelope_id = 'envelope_id_100'
event_id = 'event_id_100'
event_id = conn.event_started(token, envelope_id, event_id,
                              'etudes_django_frontend_profile_updated')
item = conn.packer.pack({'item': 'item'})
conn.item_received(token, envelope_id, event_id, 1, item)
conn.event_ended(token, envelope_id, event_id)
conn.envelope_ended(token, envelope_id)
