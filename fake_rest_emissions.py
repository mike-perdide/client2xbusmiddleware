import http.client
import json

headers = {"X-Messaging-Token": "bcd1ed6f62ce3871ae5ccfa0a3d6944c735745c6",
           "Content-type": "text/json"}


def send_request(param_dict, method, url):
    params = json.dumps(param_dict)
    conn = http.client.HTTPConnection("localhost", port="6545")
    conn.request(method, url, params, headers)

    response = conn.getresponse()
    data = response.read().decode('utf-8')
    data = json.loads(data)
    assert response.status == 200, data

    return data


print('POST /envelope')
results = send_request({}, "POST", "/envelope")

results['event_type'] = "etudes_django_frontend_profile_updated"
envelope_id = results['envelope_id']
print(results)

print('POST /event')
results = send_request(results, "POST", "/event")
print(results)

print('POST /item')
results['item'] = {}
send_request(results, "POST", "/item")
send_request(results, "POST", "/item")
send_request(results, "POST", "/item")
send_request(results, "POST", "/item")

print(results)
print('PUT /event')
send_request(results, "PUT", "/event")
print('PUT /envelope')
send_request(results, "PUT", "/envelope")

# Test full_envelope
print("Full envelope")
multi_items = {
    "multiple_event_dict": {
        "etudes_django_frontend_profile_updated": [{}],
        "etudes_django_frontend_profile_created": [{}],
    }
}
results = send_request(multi_items, "POST", "/full_envelope")
