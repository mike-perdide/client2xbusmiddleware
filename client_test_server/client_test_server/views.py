""" Cornice services.
"""
from cornice import Service


envelope = Service(name='envelope', path='/envelope')


@envelope.post()
def envelope(request):
    print("envelope post")
    return {'success': True}


event = Service(name='event', path='/event')

@event.post()
def event(request):
    print("event post")
    return {'success': True}


item = Service(name='item', path='/item')


@item.post()
def item(request):
    print("item post")
    return {'success': True}
